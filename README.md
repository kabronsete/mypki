# MyPki

This project is a gambas component that provides a complete openssl frontend with all its functions.
License is open source.

This code is a 1.0 version. Betatesters are welcome!!!

Suggestions are welcome

With this gambas class you will can

- Create complete PKI structures, Root CA, intermediateCA, CrlDistributionPoint, Alternativenames, etc.
- Manage X509 certificates. Client & Server certificates, ocsp, codesigning, timestamp, S/mime.
- Manage Pkcs12 certificates.
- Manage private keys RSA, DSA, ECDH, ECDSA, X25519, ED449.
- SSL/TLS client test.
- S/MIME for encrypted email.
- Timestamps request, generation and verification.


## Getting started

All you have to do is to include this component in your gambas application and instantiate it.
- Dim F as new MyPki
- You can read the initial manual MyPki.pdf
- You can read complete documentation of each module with each function explained and with examples. 

